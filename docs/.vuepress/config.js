module.exports = {
    title: 'Checkpoint',
    description: 'Checkpoint safety',
    base: '/checkpoint/',
    dest: 'public',
    themeConfig: {
        nav: [
          { text: 'Home', link: '/' },
          { text: 'Get Started', link: '/guide/' },
          { text: 'How it works', link: '/how/' },
          { text: 'Help', link: '/help/' },
        ],
        sidebar: 'auto'
      }
}