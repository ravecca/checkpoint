---
sidebar: auto
---

# Help

If you have trouble with the [quick setup guide](/guide/), you can [contact me](https://t.me/ravecca) for help


