---
sidebar: auto
---

# Quick Setup guide

Follow this steps to quickly test how it works...

## 1 - Copy template spreadsheet

The template has an app script in order to generate the QR Codes printouts. The sailors can be checked by QR Code, Name or Sail numbers. If you don't need QR codes, you can copy the template without the script.

[Copy the spreadsheet with dummy data](https://docs.google.com/spreadsheets/d/1yzzXhQXJaq-fuf4y3I8zKIWNX19Q3DpCMTUWmaqxDic/copy#gid=0)

## 2 - Paste entries list

On the first sheet 'Entries', paste your entries list, please don't change columns headers and columns order.
First colum, ID, is any number id, it can be bow number or any other.

## 3 - Review Config Sheet

Go to 'Config' sheet and Select Time Zone.

Change check points names if needed. 

## 4 - Copy Spreadsheet URL

Go to url bar and copy the full spreadsheet URL.

## 5 - Connecting Telegram bot

Open Telegram bot and follow instructions. First step is to paste the Spreadsheet URL you copied.

[Open the Telegram Bot](https://t.me/chkpointsBot)

On bottom left corner select Menu > Status... you should see the number of entries on each class.

