---
home: true
heroImage: ./checkpointz.png
heroText: Checkpoint
tagline: Easy safety check in / check out with Google Spreadsheets & Telegram
actionText: Get Started →
actionLink: /guide/
features:
- title: Simplicity First
  details: Minimal setup with well known Google Spreadsheets. You own your data.
- title: Telegram interface
  details: Input data is made through Telegram, making it easy for any volunteer.
- title: Performant
  details: With minimum data transfer keep your team updated and data centralized.
footer: MIT Licensed | Copyright © 2022 Diego Ravecca
---