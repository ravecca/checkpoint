---
sidebar: auto
---

# How this works

All the information is centralized on the google spreadsheet you copied to your own Google Drive. The bot is used to input and read data, and it is allowed to do that because the spreadsheet is shared with a special google service account (safety@checkpointbot.iam.gserviceaccount.com). If you want to stop sharing the spreadsheet, the bot would not be communicating anymore and will be disconnected, the same like any other user.

## Owner and collaborators

The Owner is the person who copied the template spreadsheet.

Collaborators can be volunteers interacting with the spreadsheet only through Telegram bot. Collaborators pick a checkpoint and start checking sailors.

## The spreadsheet

**Entries** is the first sheet where you paste your entries information. It will show a colour change when each sailor is checked if the checking point has an action setted.

**Log** is the master logging sheet where all checkpoints actions are stored.

**Checkpoints** is the log for collaborators selecting different checkpoints.

**Config** basic configuration options.

**Qr Doubles** is where the QR codes for double handled classes will be generated

**QR Singles** the same for single handled classes

**TimeZones** time zones list